# config valid only for current version of Capistrano
lock "3.7.2"

set :application, "uniqueway"
set :repo_url, "https://gitlab.com/tlikai/tests.git"

# Default branch is :master
ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/deploy/tests"

set :bundle_gemfile, -> { release_path.join('backend', 'Gemfile') }
set :bundle_binstubs, -> { shared_path.join('backend', 'bin') }
set :bundle_path, -> { shared_path.join('backend', 'bundle') }
set :bundle_flags, '--deployment --quiet'
set :bundle_jobs, 4

set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system', 'public/uploads')


#set :current_path, "#{current_path}"
#set :release_path, "#{release_path}/backend"

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: "log/capistrano.log", color: :auto, truncate: :auto

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# append :linked_files, "config/database.yml", "config/secrets.yml"

# Default value for linked_dirs is []
# append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5
