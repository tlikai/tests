$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "core/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "core"
  s.version     = Core::VERSION
  s.authors     = ["likai"]
  s.email       = ["youyuge@gmail.com"]
  s.homepage    = "http://www.uniqueway.com"
  s.summary     = "http://www.uniqueway.com"
  s.description = "http://www.uniqueway.com"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", ">= 5.0.0.rc2", "< 5.1"

  s.add_development_dependency "sqlite3"
end
